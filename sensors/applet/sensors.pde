int analogPin = 0;     // Pin 0 reads input values from Maxbotics LV-EZ3 sensor
int units = 0;         // Variable to store the analog value read
int inches = 0;        // Distance to nearest object, in inches

void setup()
{
  Serial.begin(19200);
  analogReference(DEFAULT);              // Set the analog reference to 5V = 1023
}

void loop()
{
  units = analogRead(analogPin);        // Read the input pin
  inches = ((units * 0.0049)/0.0098);   // (units * 0.0049) converts units to voltage, per arduino documentation
                                        // 0.0098 is the voltage/inch ratio provided by LV-EZ3 datasheet
  
  Serial.println(inches);               // debug value
  delay(500);                           // provide a short delay before reading.
}
